import Vue from 'vue';
import VueRouter from "vue-router";
import CompaniesComponent from "./components/CompaniesComponent";

Vue.use(VueRouter);




const router = new VueRouter({
    mode: 'history',
    routes : [
        {    path: '/companies/',
            query: {
                page: 0,
                country: ''
            },
            component: CompaniesComponent
        },
        {
            path: '/',
            query: {
                page: 0,
                country: ''
            },
            component: CompaniesComponent
        }
    ]
});

export default router;
