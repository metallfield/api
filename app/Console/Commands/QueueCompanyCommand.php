<?php

namespace App\Console\Commands;

use App\Company;
use App\Jobs\CompanyJob;
use App\Services\CompanyApiService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class QueueCompanyCommand extends Command
{
    public const QUEUE_SIZE_KEY = "QUEUE_SIZE";
    public const QUEUE_MAX_SIZE = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue-company {accounts} {companies}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $companies = CompanyApiService::getCompanies($this->argument('companies'));
        }catch (\Throwable $e) {
            $this->error($e->getMessage());
            return;
        }

        while (true)
        {
            $size = Cache::get(self::QUEUE_SIZE_KEY);
            if($size > self::QUEUE_MAX_SIZE) {
                sleep(1);
                continue;
            }

            $company = $companies->current();
            $companies->next();
            Cache::increment(self::QUEUE_SIZE_KEY);
            $currentCompany = ['number' => $company->number, 'title' => $company->title];

            CompanyJob::dispatch( $currentCompany, $this->argument('accounts'));

        }
    }
}
