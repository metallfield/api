<?php

namespace App\Console\Commands;

use App\Repositories\CompanyRepository;
use Illuminate\Console\Command;

class ExcelExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export-excel
     {type : type of export data, should be "officers" or "individuals"}
     {file : name of file for export, that will be writing in storage}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all officers/individuals data to excel file. Allowed data type: "officers" or "individuals" ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $type = $this->argument('type');
        $filename = $this->argument('file');
        $handle = fopen(storage_path('/app/'.$filename), 'w+');
        fputcsv($handle, [
            'name',
            'address_country',
            'country_of_residence',
            'company_title',
            'company_number'
         ]);
        try {
            CompanyRepository::getAllRecordsForExcel($type, $handle);
        }catch (\Throwable $e)
        {
            $this->line($e->getMessage());
        } finally {
            fclose($handle);
        }
    }
}
