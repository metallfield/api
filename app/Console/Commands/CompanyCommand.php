<?php

namespace App\Console\Commands;

use App\Account;
use App\Company;
use App\Exceptions\MyServerException;
use App\Repositories\AccountRepository;
use App\Repositories\CompanyRepository;
use App\Services\CompanyApiService;
use App\Services\UsersRequestService;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company {accounts} {companies}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read companies from csv and pull their officers, individuals from api';
    /**
     * @var \App\Repositories\CompanyRepository
     */
    private $companyRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $accounts = CompanyApiService::getAccounts($this->argument('accounts'));
        }catch (\Throwable $e) {
            $this->error($e->getMessage());
            return;
        }


        try {
            $companies = CompanyApiService::getCompanies($this->argument('companies'));
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return;
        }
        /** @var Company $company */


        foreach ($companies as $company){
            try {
                $company = $companies->current();
                CompanyApiService::getApiData($company, $accounts);
                $companies->next();
            }catch (\Throwable $e){
                Log::error($e->getMessage());
                $this->error($e->getMessage());
                sleep(5);
                continue;
            }
        }


    }
}
