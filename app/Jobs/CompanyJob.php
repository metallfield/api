<?php

namespace App\Jobs;

use App\Account;
use App\Company;
use App\Console\Commands\QueueCompanyCommand;
use App\Exceptions\CompaniesJobException;
use App\Repositories\CompanyRepository;
use App\Services\CompanyApiService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;


class CompanyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;

    private $company;
    private $accountsPath;

    /**
     * Create a new job instance.
     *
     *
     * @param $company
     * @param $accountsPath
     */
    public function __construct( $company, $accountsPath )
    {
        $this->delay = 5;
        $this->accountsPath = $accountsPath;
        $this->company =  $company;
    }

    /**
     * Execute the job.
     *
     *
     */
    public function handle()
    {
        $company = new Company(['number' => $this->company['number'], 'title' => $this->company['title']]);
        if (CompanyRepository::isCompanyExists($company) == false)
        {
            do{
                try {
                    $bestAccount = CompanyApiService::getAccountForJob($this->accountsPath);
                }catch (\Throwable $e){
                    throw new CompaniesJobException($e->getMessage());
                }
            }while($bestAccount == null);

            $account = new Account(['ip' => $bestAccount['ip'], 'api_key' => $bestAccount['api_key']]);
            try {

                CompanyApiService::getApiDataForJob($company, $account);
                Log::info('Creating new company '.$company->title);
            }catch (\Throwable $e){
                throw new CompaniesJobException($e->getMessage());
            } finally {
                Cache::decrement(QueueCompanyCommand::QUEUE_SIZE_KEY);
            }
        }else{
            Log::debug('Company '.$company->title.' is exist');
            Cache::decrement(QueueCompanyCommand::QUEUE_SIZE_KEY);
        }
    }
}
