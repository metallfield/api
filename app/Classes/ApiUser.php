<?php

namespace App\Classes;
class ApiUser
{

    private $name;
    private $apiKey;
    private $ip;

    public function __construct($data)
    {
        $this->name = $data[1];
        $this->ip = $data[0];
        $this->apiKey = $data[2];
    }

    public function getUserName()
    {
        return $this->name;
    }
    public function getUserIp()
    {
        return $this->ip;
    }
    public function getUserApiKey()
    {
        return $this->apiKey;
    }
}
