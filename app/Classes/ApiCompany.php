<?php


namespace App\Classes;


class ApiCompany
{

    /**
     * @var string|string[]|null
     */
    private $number;
    private $name;
    private $officers;
    private $individuals;
    public function __construct($name, $number)
    {
        $this->name = $name;
        $this->number = preg_replace('/"/', '', $number);
    }

    public function getCompanyNumber()
    {
        return $this->number;
    }

    public function getCompanyName()
    {
        return $this->name;
    }

    public function setApiData($data)
    {
        $this->officers = isset($data['officer']['items'] ) ? self::getAddressFromArray($data['officer']['items'])  : null;
        $this->individuals = isset($data['individual']['items']) ? self::getAddressFromArray($data['officer']['items']) : null;
    }

    public function getCompanyOfficers()
    {
        return $this->officers;
    }

    public function getCompanyIndividuals()
    {
        return $this->individuals;
    }
    private static function getAddressFromArray($items)
    {
        $resultArr = [];
        foreach ($items as $item)
        {
            $data['country_of_residence'] = isset($item['country_of_residence']) ? $item['country_of_residence'] : null;
            $data['address_country'] = isset($item['address']['country']) ? $item['address']['country'] : null;
            $data['name'] = $item['name'];
            $resultArr[] = $data;
        }
        return $resultArr;
    }
}
