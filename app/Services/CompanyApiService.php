<?php


namespace App\Services;


use App\Account;
use App\Company;
use App\Exceptions\MyServerException;
use App\Repositories\AccountRepository;
use App\Repositories\CompanyRepository;
use Illuminate\Database\Eloquent\Collection;


class CompanyApiService
{

    public static function getApiData(Company $company, $accounts)
    {

        $response = new UsersRequestService();

        try {
            /** @var Account $account */
            $account = $accounts->current();
            $accounts->next();

            try {
                $response->getCompanyOfficers($company, $account);
            } catch (MyServerException $exception) {
                throw new \RuntimeException('internal error exception with company ' . $company->title);
            }
            AccountRepository::addHitForIp($account->ip);

            /** @var Account $account */
            $account = $accounts->current();


            try {
                $response->getCompanyIndividuals($company, $account);
            } catch (MyServerException $exception) {
                throw new \RuntimeException('internal error exception with company ' . $company->title);
            }

            AccountRepository::addHitForIp($account->ip);
            CompanyRepository::saveCompany($company);

        } catch (\Throwable $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }

    public static function getApiDataForJob(Company $company, Account $account)
    {
        $response = new UsersRequestService();

        try {

            try {
                $response->getCompanyOfficers($company, $account);
            } catch (MyServerException $exception) {
                throw new \RuntimeException('internal error exception with company ' . $company->title);
            }
            AccountRepository::addHitForIp($account->ip);

            try {
                $response->getCompanyIndividuals($company, $account);
            } catch (MyServerException $exception) {
                throw new \RuntimeException('internal error exception with company ' . $company->title);
            }
            AccountRepository::addHitForIp($account->ip);

            CompanyRepository::saveCompany($company);

        } catch (\Throwable $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }

    public static function getAccounts(string $accountsPath)
    {
        try {
            /** @var AccountRepository $accountRepository */
            $accountRepository = app(AccountRepository::class, ['csvPath' => $accountsPath]);
            return $accountRepository->getGenerator();
        } catch (\Throwable $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }

    public static function getAccountForJob(string $accountsPath)
    {
        try {
            $accountRepository = app(AccountRepository::class, ['csvPath' => $accountsPath]);
            return $accountRepository->getBestAccount();
        } catch (\Throwable $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }

    public static function getCompanies(string $companiesPath)
    {
        try {
            /** @var CompanyRepository $companyRepository */
            $companyRepository = app(CompanyRepository::class, ['csvPath' => $companiesPath]);
            return $companyRepository->getGenerator();
        } catch (\Throwable $e) {
            throw new \RuntimeException($e->getMessage());
        }
    }
}
