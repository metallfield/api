<?php


namespace App\Services;


use App\Repositories\AccountRepository;
use App\Repositories\CsvRepository;
use App\Repositories\LimitRepository;
use Illuminate\Support\Facades\Cache;

class GetCurrentUser
{
    const ATTEMPTS_LIMIT = 600;
    private int $idx = 1;

    /**
     * @var \App\Repositories\CsvRepository
     */
    private $CsvRepository;
    /**
     * @var \App\Repositories\LimitRepository
     */
    private $limitRepository;
    /**
     * @var \App\Repositories\AccountRepository
     */
    private $accountRepository;

    public function __construct()
    {
        $this->CsvRepository = app(CsvRepository::class);
        $this->limitRepository = app(LimitRepository::class);
        $this->accountRepository = app(AccountRepository::class);
    }

    public function getCurrentUserAccount()
    {
        $this->checkForEndOfAccounts();
        $account = $this->accountRepository->getAccount($this->idx);
        return $this->checkLimits($account);
    }

    private function checkLimits($account)
    {
        $this->limitRepository->deleteOldLimits();
         $count = $this->limitRepository->getCountForIp($account->ip);
        if ($count < self::ATTEMPTS_LIMIT )
        {                 echo '<br>'. $count.'<br>';
             return  $account;
        }

        $this->idx++;
        return $this->getCurrentUserAccount();
    }

    private function checkForEndOfAccounts()
    {
        if ($this->idx == $this->accountRepository->getCountOfAccount())
        {
            $this->idx = 1;
         }
    }
}
