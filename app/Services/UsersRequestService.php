<?php


namespace App\Services;


use App\Account;
 use App\Company;
use App\Exceptions\MyServerException;
use App\Individual;
use App\Officer;
use App\Repositories\CompaniesHouseRepository;


class UsersRequestService
{

    const STATUS_SHOULD_BE = 200;
    /**
     * @var CompaniesHouseRepository
     */
    private $companiesHouseRepository;

    public function __construct()
    {
        $this->companiesHouseRepository = app(CompaniesHouseRepository::class);
    }

    public function getCompanyOfficers(Company $company, Account $account)
    {
        try {
            $response = $this->companiesHouseRepository->prepareClient('/officers', $company->number, $account);
        }catch (MyServerException $exception){
            throw new MyServerException('internal error exception');
        }

        $response = $this->checkResponse($response, $account->ip, 'officers');
         $this->filterOfficers($response, $company);
    }

    private function filterOfficers($response, Company $company)
    {
        if ($response->has('items'))
        {
            foreach ($response->get('items') as $item)
            {
                $officer = new Officer();
                $officer->name = isset($item['name']) ? $item['name'] : 'empty';
                $officer->address_country = isset($item['address']['country']) ? $item['address']['country']  : null;
                $officer->country_of_residence = isset($item['country_of_residence']) ? $item['country_of_residence'] : null;
                $officer->company()->associate($company);
                $company->officers[] = $officer;
            }
        }

    }

    private function filterIndividuals($response, Company $company)
    {

        if ($response->has('items'))
        {
            foreach ($response->get('items') as $item)
            {
                $individual = new Individual();
                $individual->name = isset($item['name']) ? $item['name'] : 'empty';
                $individual->address_country = isset($item['address']['country']) ? $item['address']['country'] : null;
                $individual->country_of_residence = isset($item['country_of_residence']) ? $item['country_of_residence'] : null;
                $individual->company()->associate($company);
                $company->individuals[] = $individual;
            }
        }

    }
    public function getCompanyIndividuals(Company $company, Account $account)
    {
        try {
            $response = $this->companiesHouseRepository->prepareClient('/persons-with-significant-control/', $company->number, $account);
        } catch (MyServerException $exception)
        {
            throw new MyServerException('internal error exception');
        }
        $response = $this->checkResponse($response, $account->ip, 'individuals');
         $this->filterIndividuals($response, $company);
    }

    private  function checkResponse($response, $ip ,string $searchObject = 'value')
    {
        if($response['status'] != self::STATUS_SHOULD_BE) {
            throw new \RuntimeException("Cant get $searchObject, too many requests, wait a second...");
        }
        return self::stringToJson($response);
    }

    private static function stringToJson($string)
    {
        return collect(json_decode($string['content'], true)) ;
    }
}
