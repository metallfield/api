<?php


namespace App\Services;


use App\Repositories\CompanyRepository;
use App\Repositories\CsvRepository;
use Illuminate\Support\Facades\Cache;
use Exception;
class GetCurrentCompany
{

    const Company_File = '/home/developer/Downloads/BasicCompanyDataAsOneFile-2020-08-01.csv';

    /**
     * @var CsvRepository
     */
    private $csvRepository;
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    public function __construct()
    {
        $this->csvRepository = app(CsvRepository::class);
        $this->companyRepository = app(CompanyRepository::class);
    }

    public function getCurrentCompany()
    {
        $company_idx = Cache::get('cidx') ? Cache::get('cidx') : 1;
        $this->checkForEndOfCompanies($this->csvRepository->csvRow(self::Company_File, $company_idx));
        $company = explode(',', $this->csvRepository->csvRow(self::Company_File, $company_idx));
        return $this->checkForExists($company);
    }

    private function checkForExists($company)
    {
        if ($this->companyRepository->getCompanyByNumber($company[1]) !== null){
            Cache::increment('cidx');
            return $this->getCurrentCompany();
        }
        return $company;
    }

    private function checkForEndOfCompanies($company)
    {
        if (empty($company))
        {
            throw new Exception('companies is over');
        }
        return $company;
    }
}
