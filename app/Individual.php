<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Individual extends Model
{

    protected $fillable = ['name', 'address_country', 'country_of_residence', 'company_id'];
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
