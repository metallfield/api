<?php

namespace App;

use Illuminate\Support\Fluent;

/**
 * Class Account
 * @package App
 *
 * @property-read string $ip
 * @property-read string $api_key
 */
class Account extends Fluent
{
}
