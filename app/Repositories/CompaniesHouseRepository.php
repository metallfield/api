<?php


namespace App\Repositories;


use App\Account;
use App\Exceptions\MyServerException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class CompaniesHouseRepository
{
    const BASE_API_URL = 'https://api.companieshouse.gov.uk/company/';
    //   const TEST_URL = 'http://api.loc/some-api';

    public  function prepareClient($link, $number, Account $account)
    {
        $client = new Client();
        try {
            $response = $client->request('GET',
                self::BASE_API_URL.$number.$link,
                //self::TEST_URL,
                //  ['headers' => ['authorization' => env('COMPANIES_HOUSE_API_KEY'), 'Content-Type' => 'application/json'],
                ['headers' => ['authorization' => $account->api_key],
                    'curl' => [CURLOPT_INTERFACE => $account->ip]
                ]);
        } catch (TooManyRequestsHttpException $e) {
            return ['status' => 429];
        } catch (ClientException $e) {
            return ['content' => null, 'status' => 200];
        }catch (ServerException $exception) {
            throw new MyServerException('internal error exception');
        }
        return ['content' => $response->getBody()->getContents(), 'status' => 200];
    }
}
