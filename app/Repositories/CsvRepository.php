<?php


namespace App\Repositories;


class CsvRepository
{

    /**
     * @var false|resource
     */


    public function csvRow($file,$offset)
    {
        $file = new \SplFileObject($file);
          $file->seek($offset);
          return $file->current();
    }

    public  function writeTo($path, $count)
    {
        if (filesize($path) == 0)
        {
            $fp = fopen($path, 'w');
            for ($i = 1; $i <=$count; $i++){
                $arr = ['ip' =>
                    '127.0.0.'.$i,
                    'name' =>
                        self::generateName(),
                    'password' =>
                        self::generatePassword(),
                    'api_key' =>
                        self::generateApiKey()

                ];
                fputcsv($fp, $arr);
            }
            fclose($fp);
        }
    }

    private static function generateName()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }
    private static function generatePassword()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }

    private static function generateApiKey()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 10);
    }



}
