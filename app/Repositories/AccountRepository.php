<?php


namespace App\Repositories;


use App\Account;
use App\Limit;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use \RuntimeException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use League\Csv\Exception;

class AccountRepository
{
    private $csvPath;

    private const LIMIT_TIME_INTERVAL = 5;
    private const LIMIT_HITS = 600;

    /**
     * @var array
     */
    private $accounts;

    public function __construct(string $csvPath)
    {
        $this->csvPath = $csvPath;
        $this->checkCsvPath();

        $this->loadAccounts();
        $this->refreshHits();
    }

    public function rows($path)
    {
        $file = fopen($path, 'r');
        while (!feof($file)) {
            $row = fgetcsv($file, 4096);

            yield $row;
        }
        return;
    }

    private function loadAccounts()
    {
        foreach ($this->rows($this->getAccountsCSVPath()) as $row)
        {
            if(empty($row)) {
                continue;
            }

            @list($ip, $api_key) = $row;
            if (empty($ip)) {
                throw new \RuntimeException('IP is not provided');
            }
            if (empty($api_key)) {
                throw new \RuntimeException('API key is not provided');
            }

            $hits = 0;

            $this->accounts[] = compact('ip', 'api_key', 'hits');
        }
    }

    private function getAccountsCSVPath(): string
    {
        return storage_path('app/' . $this->csvPath);
    }

    private function checkCsvPath()
    {
        if (!Storage::exists($this->csvPath)) {
            throw new FileNotFoundException(sprintf('File "%s" not found', storage_path('app/'.$this->csvPath)));
        }
    }

    public function getGenerator()
    {
        while (true) {
            $found = false;
            foreach ($this->accounts as $account) {
                if ($account['hits'] < self::LIMIT_HITS) {
                    $found = true;
                    yield new Account($account);
                }
            }

            if(!$found) {
                sleep(10);
            }

            $this->refreshHits();
        }
    }

    public function getBestAccount()
    {
        $data = collect($this->accounts);
        if ($data->min('hits') >= self::LIMIT_HITS)
        {
            return null;
        }
        return $data->where('hits', $data->min('hits'))->first();
    }

    public function cleanOldHits()
    {
        Limit::where('created_at', '<', Carbon::now()->subMinutes(self::LIMIT_TIME_INTERVAL))->delete();
    }

    private function refreshHits()
    {
        $this->cleanOldHits();

        $hits = Limit::query()->groupBy('ip')->select(
            'ip',
            DB::raw('count(*) as count')
        )
            ->pluck('count', 'ip');

        foreach($this->accounts as &$account) {
            if(isset($hits[$account['ip']])) {
                $account['hits'] = $hits[$account['ip']];
            }
        }
    }

    public static function addHitForIp($ip)
    {
        Limit::create(['ip'=> $ip]);
    }
}
