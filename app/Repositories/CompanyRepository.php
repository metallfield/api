<?php


namespace App\Repositories;


use App\Company;
use App\Individual;
use App\Officer;
use http\Exception\RuntimeException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\CannotWriteFileException;

class CompanyRepository
{
    private $csvPath;


    public function __construct(string $csvPath)
    {
        $this->csvPath = $csvPath;
        $this->checkCsvPath();
    }

    public static function saveCompany(Company $company)
    {
        if (!$company->save())
        {
            throw new \RuntimeException('save error with company '.$company->title);
        }
        foreach ($company->getRelations() as $models)
        {
            $models = $models instanceof Collection
                ? $models->all() : [$models];

            foreach (array_filter($models) as $model) {
                $model->company_id = $company->id;
                if (!$model->save()) {
                    throw new \RuntimeException('save error with company '.$company->title);
                }
            }
        }
    }

    public static function searchCountries(string $search , string $type)
    {
        $address_country = DB::table($type)
            ->select('address_country')
            ->groupBy('address_country')
            ->where('address_country', 'like', "%$search%")
            ->pluck('address_country');
        $country_of_residence = DB::table($type)
            ->select('country_of_residence')
            ->groupBy('country_of_residence')
            ->where('country_of_residence', 'like', "%$search%")
            ->pluck('country_of_residence');
        return collect($address_country)->merge($country_of_residence);
    }

    public function getGenerator()
    {
        $file = fopen($this->getCSVPath(), 'r');
        fgetcsv($file, 4096);
        while (!feof($file)) {
            $row = fgetcsv($file, 4096);


            if (empty($row))
            {
                continue;
            }
            if (empty($row[0]))
            {
                throw new \RuntimeException('company name is not provided');
            }
            if (empty($row[1]))
            {
                throw new \RuntimeException('company number is not provided');
            }
            $company = new Company([
                'number' => $row[1],
                'title' => $row[0],
            ]);
            if( self::isCompanyExists($company)) {
                continue;
            }
            yield $company;
        }
    }

    private function getCSVPath(): string
    {
        return storage_path('app/' . $this->csvPath);
    }

    private function checkCsvPath()
    {
        if (!Storage::exists($this->csvPath)) {
            throw new FileNotFoundException(sprintf('File "%s" not found', storage_path('app/'.$this->csvPath)));
        }
    }

    public static function isCompanyExists(Company $company): bool
    {
        return  Company::where('number', $company->number)->exists();
    }

    public static function getAllCompanies($country = null, $type = null)
    {
        if ($country != null && $type != null)
        {
            $ids = DB::table($type)
                ->select('company_id')
                ->where('address_country', $country)
                ->orWhere('country_of_residence', $country)
                ->groupBy('company_id')
                ->paginate(8);

            return [
                'last_page' => $ids->lastPage(),
                'data' => Company::select('title', 'number', 'id')->find(collect($ids->items())->pluck('company_id'))
            ];
        }
        return Company::select('title', 'number', 'id')->paginate(8);
    }


    public static function getAllCountries()
    {
        $address_country = Officer::groupBy('address_country')->select('address_country')->pluck('address_country');
        $country_of_residence  = Officer::groupBy('country_of_residence')->select('country_of_residence')->pluck('country_of_residence');
        $individuals_address_country = Individual::groupBy('address_country')->select('address_country')->pluck('address_country');
        $individuals_country_of_residence = Individual::groupBy('country_of_residence')->select('country_of_residence')->pluck('country_of_residence');
        $result = collect($address_country)->merge($country_of_residence)->merge($individuals_address_country)->merge($individuals_country_of_residence)->unique();
        return $result->filter();
    }

    public static function getCompanyOfficers(Company $company)
    {
        return $company->officers;
    }

    public static function getCompanyIndividuals(Company $company)
    {
        return $company->individuals;
    }

    public static function getAllCompaniesForExport($country, $type, $handle)
    {
        DB::table($type)
            ->select('company_id')
            ->where('address_country', $country)
            ->orWhere('country_of_residence', $country)
            ->orderBy('company_id')
            ->chunk(100, function ($ids) use ($handle) {
                $companies = Company::with('officers', 'individuals')
                    ->find(collect($ids)->pluck('company_id'));
                foreach ($companies as $company)
                {
                    self::dataToCsv($handle, $company,  $company['officers'], 'officer');
                    self::dataToCsv($handle, $company,  $company['individuals'], 'individual');
                }
            });
    }

    public static function exportToCsv($country, $type)
    {

        $filename = "company-export.csv";
        try {
            $handle = fopen(storage_path('/app/'.$filename), 'w+');
            fputcsv($handle, ['company title',
                'company number',
                'type',
                'name',
                'address_country',
                'country_of_residence']);
            self::getAllCompaniesForExport($country, $type, $handle);
        }catch (CannotWriteFileException $e){
            throw new \RuntimeException('can`t write this file');
        }catch (\Throwable $e){
            throw new \RuntimeException('can`t write this file');
        } finally {
            fclose($handle);
        }
    }

    private static function dataToCsv($handle, Company $company, $data, string $type)
    {

        foreach (collect($data) as $datum)
        {
            try {
                fputcsv($handle, [
                    $company->title,
                    $company->number,
                    $type,
                    $datum->name,
                    $datum->address_country,
                    $datum->country_of_residence
                ]);
            }catch (CannotWriteFileException $e){
                throw new \RuntimeException('can`t write this file');
            }
        }
    }


    /**
     * export all officers/individuals data to excel file
     * @param string $type
     * @param $handle
     */
    public static function getAllRecordsForExcel(string $type, $handle)
    {
        DB::table($type)

            ->select('name', 'address_country', 'country_of_residence', 'companies.title', 'companies.number')
            ->join('companies', 'companies.id', '=', 'company_id')
             ->orderBy('company_id')
             ->chunk(500, function ($records) use($handle){
                try {
                    self::dataToExcel($records,$handle);
                }catch (\Throwable $e)
                {
                    throw new \RuntimeException($e->getMessage());
                }

            });
    }

    private static function dataToExcel($data , $handle)
    {
        $records = collect($data);
         foreach ($records as $record)
        {
            try {
                fputcsv($handle, [
                    $record->name,
                    $record->address_country,
                    $record->country_of_residence,
                    $record->title,
                    $record->number,
                 ]);
            }catch (CannotWriteFileException $e)
            {
                throw new \RuntimeException('cannot write this file');
            }

        }
    }
}
