<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{

    /**
     * Class Officer
     * @package  App
     * @property string address_country
     * @property string country_of_residence
     */
    protected $fillable = ['name', 'address_country', 'country_of_residence', 'company_id'];
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}

