<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Throwable;

class CompaniesJobException extends Exception
{
    public function report()
    {
        Log::error($this->getMessage());
    }

}
