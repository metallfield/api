<?php


namespace App\Exceptions;


class MyServerException extends \Exception
{

    /**
     * MyServerException constructor.
     * @param string $string
     */
    public function __construct(string $string)
    {
    }
}
