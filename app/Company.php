<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App
 *
 * @property string $title
 * @property string $number
 */
class Company extends Model
{
    protected $fillable = ['title', 'number'];

    public function officers()
    {
        return $this->hasMany(Officer::class);
    }

    public function individuals()
    {
        return $this->hasMany(Individual::class);
    }
}
