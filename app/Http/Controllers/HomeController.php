<?php

namespace App\Http\Controllers;

use App\Repositories\CompanyRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {

         return redirect('/companies');
    }

    public function home()
    {
        return view('home');
    }
}
