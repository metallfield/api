<?php

namespace App\Http\Controllers;

use App\Company;
use App\Repositories\CompanyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{



    public function getAllCompanies( $country = null, $type = null)
    {
        $companies = CompanyRepository::getAllCompanies($country, $type);
        return response()->json($companies);
    }

    public function getCompanyOfficers(Company $company)
    {
        $officers = CompanyRepository::getCompanyOfficers($company);
        return response()->json($officers);
    }

    public function getCompanyIndividuals(Company $company)
    {
        $individuals = CompanyRepository::getCompanyIndividuals($company);
        return response()->json($individuals);
    }

    public function getAllCountries()
    {
        $countries = CompanyRepository::getAllCountries();
        return response()->json($countries);
    }


    public function exportCompanies($country, $type)
    {
        try {
            CompanyRepository::exportToCsv($country, $type);

            $headers = [
                'Content-Type' => 'text/csv',
            ];
            return Storage::download('company-export.csv', "company-export.$country.csv", $headers);
        }catch (\Throwable $e){
            return response()->json( ['error' => $e->getMessage()]);
        }

    }

    public function searchCountries(string $search, string $type)
    {
        $countries = CompanyRepository::searchCountries($search, $type);
        return response()->json($countries);
    }
}
