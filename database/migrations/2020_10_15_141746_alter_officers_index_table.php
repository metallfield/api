<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOfficersIndexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('officers', function (Blueprint $table){
           $table->index('company_id');
           $table->index(['company_id', 'address_country', 'country_of_residence'],
               'officers_company_id_address_country_country_of_residence_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('officers', function (Blueprint $table){
           $table->dropIndex('company_id');
           $table->dropIndex('officers_company_id_address_country_country_of_residence_index');
        });
    }
}
