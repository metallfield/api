<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');




Route::get('/some-request', 'SomeUsersRequestController@someRequest');

Auth::routes();

Route::get('/companies/{company}/officers', 'CompanyController@getCompanyOfficers');
Route::get('/companies/{company}/individuals', 'CompanyController@getCompanyIndividuals');
Route::get('/companies/countries', 'CompanyController@getAllCountries');
Route::get('/countries/{search}/{type}', 'CompanyController@searchCountries');
Route::get('/get-companies/{country?}/{type?}', 'CompanyController@getAllCompanies');
Route::get('/get-companies/export/{country}/{type}', 'CompanyController@ExportCompanies');

Route::get('/{any}', 'HomeController@home')->where('any', '.*');
